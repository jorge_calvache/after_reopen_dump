WITH pilot as (
        select upper(case
                         when substr(lower(trim(C.country)), 1, 2) not in
                              ('ar', 'br', 'cl', 'co', 'cr', 'ec', 'mx', 'pe', 'uy') then null
                         else substr(lower(trim(C.country)), 1, 2) end) as country,
               try_to_numeric(order_id)                                 as order_id,
               coalesce(C.CUSTOM:ticketAutomationExitRuleStr::text,
                        C.CUSTOM:dMexitruleTree::text)                  as exit_rule
        from FIVETRAN.CO_PG_MS_KUSTOMER_ETL_PUBLIC.CONVERSATIONS C
        where  exit_rule is not null

    ),
          pick_up as (
	select 'AR' COUNTRY,	 ORDER_ID,	 DELIVERY_METHOD,	 date_trunc(day,	 created_at)::date day from ar_core_orders_public.delivery_order
		where date_trunc(month,	 created_at)::date between current_date - interval '5 months' and current_date - interval '1 day'  union all
	select 'BR' COUNTRY,     ORDER_ID,	 DELIVERY_METHOD,	 date_trunc(day,	 created_at)::date day from br_core_orders_public.delivery_order
		where date_trunc(month,	 created_at)::date between current_date - interval '5 months' and current_date - interval '1 day'  union all
	select 'EC' COUNTRY,	 ORDER_ID,	 DELIVERY_METHOD,	 date_trunc(day,	 created_at)::date day from ec_core_orders_public.delivery_order
		where date_trunc(month,	 created_at)::date between current_date - interval '5 months' and current_date - interval '1 day'  union all
	select 'PE' COUNTRY,	 ORDER_ID,	 DELIVERY_METHOD,	 date_trunc(day,	 created_at)::date day from pe_core_orders_public.delivery_order
		where date_trunc(month,	 created_at)::date between current_date - interval '5 months' and current_date - interval '1 day' union all
	select 'MX' COUNTRY,	 ORDER_ID,	 DELIVERY_METHOD,	 date_trunc(day, created_at)::date day from mx_core_orders_public.delivery_order
		where date_trunc(month,	 created_at)::date between current_date - interval '5 months' and current_date - interval '1 day'  union all
	select 'CL' COUNTRY,	 ORDER_ID,	 DELIVERY_METHOD,	 date_trunc(day,	 created_at)::date day from cl_core_orders_public.delivery_order
		where date_trunc(month,	 created_at)::date between current_date - interval '5 months' and current_date - interval '1 day'  union all
	select 'CO' COUNTRY,	 ORDER_ID,	 DELIVERY_METHOD,	 date_trunc(day,	 created_at)::date day from co_core_orders_public.delivery_order
		where date_trunc(month,	 created_at)::date between current_date - interval '5 months' and current_date - interval '1 day'  union all
	select 'UY' COUNTRY,	 ORDER_ID,	 DELIVERY_METHOD,	 date_trunc(day,	 created_at)::date day from uy_core_orders_public.delivery_order
		where date_trunc(month,	 created_at)::date between current_date - interval '5 months' and current_date - interval '1 day' union all
	select 'CR' COUNTRY,	 ORDER_ID,	 DELIVERY_METHOD,	 date_trunc(day,	 created_at)::date day from cr_core_orders_public.delivery_order
		where date_trunc(month,	 created_at)::date between current_date - interval '5 months' and current_date - interval '1 day'
),


          COMPENSATIONS AS (
              SELECT *,
                     row_number() over (partition by cs.country, cs.order_id order by cs.ticket_created_at) as rw
              FROM OPS_GLOBAL.CS_METRICS_CONSOLIDATED CS
              WHERE CS.IS_DEFECT
                AND CS.USER_TYPE = 'Customer'
                and CS.type ilike '%Non-Live%'
                  QUALIFY RW = 1
          ),
          COUNTRIES_UNION_ALL AS (
              select 'AR'                                                            COUNTRY,
                     CITY_NAME,
                     AR_REACTIVES_ROI_CHURN_V2.CREATED_AT::date as created_at,
                     AR_REACTIVES_ROI_CHURN_V2.ORDER_ID,
                     CS.TICKET_URL,
                     TICKET_TYPE,
                     TIPOLOGIA,
                     AR_REACTIVES_ROI_CHURN_V2.IS_AUTOMATED,
                     AR_REACTIVES_ROI_CHURN_V2.VERTICAL_GROUP,
                     AR_REACTIVES_ROI_CHURN_V2.VERTICAL_SUB_GROUP,
                     AR_REACTIVES_ROI_CHURN_V2.RFM_SEGMENT
                            AS SEGMENT_RFM,
                     AR_REACTIVES_ROI_CHURN_V2.FINANCE_USER_TYPE,
                     AR_REACTIVES_ROI_CHURN_V2.ITERATION   as treatment,
                     AR_REACTIVES_ROI_CHURN_V2.LEVEL_1 AS CATEGORY_LEVEL_1,
                     AR_REACTIVES_ROI_CHURN_V2.LEVEL_2             as SUB_CATEGORY_LEVEL_1,
                     AR_REACTIVES_ROI_CHURN_V2.APPLICATION_USER_ID                   as APPLICATION_USER_ID,
                     cs.LAST_AGENT_EMAIL  as last_agent,

                     CS.SOLUTION_TYPE,
                     TICKETAUTOMATIONEXITRULE as exit_rule,
                     exit_tipification,
go.payment_method, go.delivery_type, pick_up.delivery_method,
                  /*CASE WHEN GIVEN_VALUE_AUTOMATIC_ > 0 AND MONTO_COMPENSADO_LIVE - GIVEN_VALUE_AUTOMATIC_ > 0 THEN 'AUTOMATIC-MANUAL_COMPENSATION'
                          WHEN GIVEN_VALUE_AUTOMATIC_ > 0 THEN 'AUTOMATIC_COMPENSATION'
                          WHEN MONTO_COMPENSADO_LIVE - GIVEN_VALUE_AUTOMATIC_ > 0 THEN 'MANUAL_COMPENSATION'
                          ELSE NULL END AS TYPE_COMPENSATION,*/
                     SUM(NSS_X_RATINGS) / SUM(NO_OF_RATINGS)                      AS NSS,
                     SUM(AR_REACTIVES_ROI_CHURN_V2.REOPEN)                           AS REOPEN,
                     SUM(0)                                          AS CLAIMED_VALUE,
                     SUM(COMPENSATION_VALUE_USD)                                   as USD_COMPENSATIONS,
                     SUM(AUTOMATIC_COMPENSATION_USD)                                  AS AUTOMATIC_COMPENSATION,
                     SUM(ALLY_COMPENSATIONS)
                                            AS PAID_BY_ALLY,
                     SUM(AGENT_COMPENSATION_USD)
    AS MANUAL_COMPENSATION,
                     SUM(ORDERS)                                                  AS ORDERS,
                     SUM(CASE WHEN TICKET_TYPE <> 'Sin Ticket' THEN 1 ELSE 0 END) AS DEFECTS,
                     (SUM(GMV_AFTER_7_DAYS / TRM.TRM)) / SUM(ORDERS)             AS GMV_AFTER_7_DAYS,
                     (SUM(GMV_AFTER_7_DAYS_ORGANIC / TRM.TRM)) / SUM(ORDERS)     AS GMV_AFTER_7_DAYS_ORGANIC,
                     (SUM(GMV_AFTER_28_DAYS / TRM.TRM)) / SUM(ORDERS)            AS GMV_AFTER_28_DAYS,
                     (SUM(GMV_AFTER_28_DAYS_ORGANIC / TRM.TRM)) / SUM(ORDERS)    AS GMV_AFTER_28_DAYS_ORGANIC,
                     SUM(ORDER_MODIFICATION_DISCOUNTS)
                                           AS FREE_DELIVERY,
                     SUM(GO.TOTAL_VALUE / TRM.TRM)                                      AS TOTAL_VALUE_USD

              from ops_global.AR_REACTIVES_ROI_CHURN_V2
                       left join COMPENSATIONS cs on cs.ORDER_ID = AR_REACTIVES_ROI_CHURN_V2.ORDER_ID and cs.COUNTRY = 'AR'
                       left join pilot on pilot.ORDER_ID = AR_REACTIVES_ROI_CHURN_V2.ORDER_ID and pilot.COUNTRY = 'AR'
                       left join ops_global.global_orders go
                                 on go.ORDER_ID = AR_REACTIVES_ROI_CHURN_V2.ORDER_ID and go.COUNTRY = 'AR' and go.COUNT_TO_GMV
                            left join pick_up on pick_up.ORDER_ID = AR_REACTIVES_ROI_CHURN_V2.ORDER_ID and pick_up.COUNTRY = 'AR'
                            left join global_finances.TRM_FIXED trm on trm.COUNTRY_CODE = 'AR'

              where date_trunc(day, AR_REACTIVES_ROI_CHURN_V2.CREATED_AT)::date between current_date - interval '5 months' and current_date - interval '1 day'
              group by 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 ,23

              union all

              select 'BR'                                                            COUNTRY,
                     CITY_NAME,
                     BR_REACTIVES_ROI_CHURN_V2.CREATED_AT::date as created_at,
                     BR_REACTIVES_ROI_CHURN_V2.ORDER_ID,
                     CS.TICKET_URL,
                     TICKET_TYPE,
                     TIPOLOGIA,
                     BR_REACTIVES_ROI_CHURN_V2.IS_AUTOMATED,
                     BR_REACTIVES_ROI_CHURN_V2.VERTICAL_GROUP,
                     BR_REACTIVES_ROI_CHURN_V2.VERTICAL_SUB_GROUP,
                     BR_REACTIVES_ROI_CHURN_V2.RFM_SEGMENT
                            AS SEGMENT_RFM,
                     BR_REACTIVES_ROI_CHURN_V2.FINANCE_USER_TYPE,
                     BR_REACTIVES_ROI_CHURN_V2.ITERATION   as treatment,
                     BR_REACTIVES_ROI_CHURN_V2.LEVEL_1 AS CATEGORY_LEVEL_1,
                     BR_REACTIVES_ROI_CHURN_V2.LEVEL_2             as SUB_CATEGORY_LEVEL_1,
                     BR_REACTIVES_ROI_CHURN_V2.APPLICATION_USER_ID                   as APPLICATION_USER_ID,
                     cs.LAST_AGENT_EMAIL  as last_agent,

                     CS.SOLUTION_TYPE,
                     TICKETAUTOMATIONEXITRULE as exit_rule,
                     exit_tipification,
go.payment_method, go.delivery_type, pick_up.delivery_method,
                  /*CASE WHEN GIVEN_VALUE_AUTOMATIC_ > 0 AND MONTO_COMPENSADO_LIVE - GIVEN_VALUE_AUTOMATIC_ > 0 THEN 'AUTOMATIC-MANUAL_COMPENSATION'
                          WHEN GIVEN_VALUE_AUTOMATIC_ > 0 THEN 'AUTOMATIC_COMPENSATION'
                          WHEN MONTO_COMPENSADO_LIVE - GIVEN_VALUE_AUTOMATIC_ > 0 THEN 'MANUAL_COMPENSATION'
                          ELSE NULL END AS TYPE_COMPENSATION,*/
                     SUM(NSS_X_RATINGS) / SUM(NO_OF_RATINGS)                      AS NSS,
                     SUM(BR_REACTIVES_ROI_CHURN_V2.REOPEN)                           AS REOPEN,
                     SUM(0)                                          AS CLAIMED_VALUE,
                     SUM(COMPENSATION_VALUE_USD)                                   as USD_COMPENSATIONS,
                     SUM(AUTOMATIC_COMPENSATION_USD)                                  AS AUTOMATIC_COMPENSATION,
                     SUM(ALLY_COMPENSATIONS)
                                            AS PAID_BY_ALLY,
                     SUM(AGENT_COMPENSATION_USD)
    AS MANUAL_COMPENSATION,
                     SUM(ORDERS)                                                  AS ORDERS,
                     SUM(CASE WHEN TICKET_TYPE <> 'Sin Ticket' THEN 1 ELSE 0 END) AS DEFECTS,
                     (SUM(GMV_AFTER_7_DAYS / TRM.TRM)) / SUM(ORDERS)             AS GMV_AFTER_7_DAYS,
                     (SUM(GMV_AFTER_7_DAYS_ORGANIC / TRM.TRM)) / SUM(ORDERS)     AS GMV_AFTER_7_DAYS_ORGANIC,
                     (SUM(GMV_AFTER_28_DAYS / TRM.TRM)) / SUM(ORDERS)            AS GMV_AFTER_28_DAYS,
                     (SUM(GMV_AFTER_28_DAYS_ORGANIC / TRM.TRM)) / SUM(ORDERS)    AS GMV_AFTER_28_DAYS_ORGANIC,
                     SUM(ORDER_MODIFICATION_DISCOUNTS)
                                           AS FREE_DELIVERY,
                     SUM(GO.TOTAL_VALUE / TRM.TRM)                                      AS TOTAL_VALUE_USD
              from ops_global.BR_REACTIVES_ROI_CHURN_V2
                       left join COMPENSATIONS cs on cs.ORDER_ID = BR_REACTIVES_ROI_CHURN_V2.ORDER_ID and cs.COUNTRY = 'BR'
                       left join pilot on pilot.ORDER_ID = BR_REACTIVES_ROI_CHURN_V2.ORDER_ID and pilot.COUNTRY = 'BR'
                       left join ops_global.global_orders go
                                 on go.ORDER_ID = BR_REACTIVES_ROI_CHURN_V2.ORDER_ID and go.COUNTRY = 'BR' and go.COUNT_TO_GMV
                            left join pick_up on pick_up.ORDER_ID = BR_REACTIVES_ROI_CHURN_V2.ORDER_ID and pick_up.COUNTRY = 'BR'
                            left join global_finances.TRM_FIXED trm on trm.COUNTRY_CODE = 'BR'

              where date_trunc(day, BR_REACTIVES_ROI_CHURN_V2.CREATED_AT)::date between current_date - interval '5 months' and current_date - interval '1 day'
              group by 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 ,23


              union all

              select 'CO'                                                            COUNTRY,
                     CITY_NAME,
                     CO_REACTIVES_ROI_CHURN_V2.CREATED_AT::date as created_at,
                     CO_REACTIVES_ROI_CHURN_V2.ORDER_ID,
                     CS.TICKET_URL,
                     TICKET_TYPE,
                     TIPOLOGIA,
                     CO_REACTIVES_ROI_CHURN_V2.IS_AUTOMATED,
                     CO_REACTIVES_ROI_CHURN_V2.VERTICAL_GROUP,
                     CO_REACTIVES_ROI_CHURN_V2.VERTICAL_SUB_GROUP,
                     CO_REACTIVES_ROI_CHURN_V2.RFM_SEGMENT
                            AS SEGMENT_RFM,
                     CO_REACTIVES_ROI_CHURN_V2.FINANCE_USER_TYPE,
                     CO_REACTIVES_ROI_CHURN_V2.ITERATION   as treatment,
                     CO_REACTIVES_ROI_CHURN_V2.LEVEL_1 AS CATEGORY_LEVEL_1,
                     CO_REACTIVES_ROI_CHURN_V2.LEVEL_2             as SUB_CATEGORY_LEVEL_1,
                     CO_REACTIVES_ROI_CHURN_V2.APPLICATION_USER_ID                   as APPLICATION_USER_ID,
                     cs.LAST_AGENT_EMAIL  as last_agent,

                     CS.SOLUTION_TYPE,
                     TICKETAUTOMATIONEXITRULE as exit_rule,
                     exit_tipification,
go.payment_method, go.delivery_type, pick_up.delivery_method,
                  /*CASE WHEN GIVEN_VALUE_AUTOMATIC_ > 0 AND MONTO_COMPENSADO_LIVE - GIVEN_VALUE_AUTOMATIC_ > 0 THEN 'AUTOMATIC-MANUAL_COMPENSATION'
                          WHEN GIVEN_VALUE_AUTOMATIC_ > 0 THEN 'AUTOMATIC_COMPENSATION'
                          WHEN MONTO_COMPENSADO_LIVE - GIVEN_VALUE_AUTOMATIC_ > 0 THEN 'MANUAL_COMPENSATION'
                          ELSE NULL END AS TYPE_COMPENSATION,*/
                     SUM(NSS_X_RATINGS) / SUM(NO_OF_RATINGS)                      AS NSS,
                     SUM(CO_REACTIVES_ROI_CHURN_V2.REOPEN)                           AS REOPEN,
                     SUM(0)                                          AS CLAIMED_VALUE,
                     SUM(COMPENSATION_VALUE_USD)                                   as USD_COMPENSATIONS,
                     SUM(AUTOMATIC_COMPENSATION_USD)                                  AS AUTOMATIC_COMPENSATION,
                     SUM(ALLY_COMPENSATIONS)
                                            AS PAID_BY_ALLY,
                     SUM(AGENT_COMPENSATION_USD)
    AS MANUAL_COMPENSATION,
                     SUM(ORDERS)                                                  AS ORDERS,
                     SUM(CASE WHEN TICKET_TYPE <> 'Sin Ticket' THEN 1 ELSE 0 END) AS DEFECTS,
                     (SUM(GMV_AFTER_7_DAYS / TRM.TRM)) / SUM(ORDERS)             AS GMV_AFTER_7_DAYS,
                     (SUM(GMV_AFTER_7_DAYS_ORGANIC / TRM.TRM)) / SUM(ORDERS)     AS GMV_AFTER_7_DAYS_ORGANIC,
                     (SUM(GMV_AFTER_28_DAYS / TRM.TRM)) / SUM(ORDERS)            AS GMV_AFTER_28_DAYS,
                     (SUM(GMV_AFTER_28_DAYS_ORGANIC / TRM.TRM)) / SUM(ORDERS)    AS GMV_AFTER_28_DAYS_ORGANIC,
                     SUM(ORDER_MODIFICATION_DISCOUNTS)
                                           AS FREE_DELIVERY,
                     SUM(GO.TOTAL_VALUE / TRM.TRM)                                      AS TOTAL_VALUE_USD
              from ops_global.CO_REACTIVES_ROI_CHURN_V2
                       left join COMPENSATIONS cs on cs.ORDER_ID = CO_REACTIVES_ROI_CHURN_V2.ORDER_ID and cs.COUNTRY = 'CO'
                       left join pilot on pilot.ORDER_ID = CO_REACTIVES_ROI_CHURN_V2.ORDER_ID and pilot.COUNTRY = 'CO'
                       left join ops_global.global_orders go
                                 on go.ORDER_ID = CO_REACTIVES_ROI_CHURN_V2.ORDER_ID and go.COUNTRY = 'CO' and go.COUNT_TO_GMV
                            left join pick_up on pick_up.ORDER_ID = CO_REACTIVES_ROI_CHURN_V2.ORDER_ID and pick_up.COUNTRY = 'CO'
                            left join global_finances.TRM_FIXED trm on trm.COUNTRY_CODE = 'CO'

              where date_trunc(day, CO_REACTIVES_ROI_CHURN_V2.CREATED_AT)::date between current_date - interval '5 months' and current_date - interval '1 day'
              group by 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23

              union all

              select 'CL'                                                            COUNTRY,
                     CITY_NAME,
                     CL_REACTIVES_ROI_CHURN_V2.CREATED_AT::date as created_at,
                     CL_REACTIVES_ROI_CHURN_V2.ORDER_ID,
                     CS.TICKET_URL,
                     TICKET_TYPE,
                     TIPOLOGIA,
                     CL_REACTIVES_ROI_CHURN_V2.IS_AUTOMATED,
                     CL_REACTIVES_ROI_CHURN_V2.VERTICAL_GROUP,
                     CL_REACTIVES_ROI_CHURN_V2.VERTICAL_SUB_GROUP,
                     CL_REACTIVES_ROI_CHURN_V2.RFM_SEGMENT
                            AS SEGMENT_RFM,
                     CL_REACTIVES_ROI_CHURN_V2.FINANCE_USER_TYPE,
                     CL_REACTIVES_ROI_CHURN_V2.ITERATION   as treatment,
                     CL_REACTIVES_ROI_CHURN_V2.LEVEL_1 AS CATEGORY_LEVEL_1,
                     CL_REACTIVES_ROI_CHURN_V2.LEVEL_2             as SUB_CATEGORY_LEVEL_1,
                     CL_REACTIVES_ROI_CHURN_V2.APPLICATION_USER_ID                   as APPLICATION_USER_ID,
                     cs.LAST_AGENT_EMAIL  as last_agent,

                     CS.SOLUTION_TYPE,
                     TICKETAUTOMATIONEXITRULE as exit_rule,
                     exit_tipification,
go.payment_method, go.delivery_type, pick_up.delivery_method,
                  /*CASE WHEN GIVEN_VALUE_AUTOMATIC_ > 0 AND MONTO_COMPENSADO_LIVE - GIVEN_VALUE_AUTOMATIC_ > 0 THEN 'AUTOMATIC-MANUAL_COMPENSATION'
                          WHEN GIVEN_VALUE_AUTOMATIC_ > 0 THEN 'AUTOMATIC_COMPENSATION'
                          WHEN MONTO_COMPENSADO_LIVE - GIVEN_VALUE_AUTOMATIC_ > 0 THEN 'MANUAL_COMPENSATION'
                          ELSE NULL END AS TYPE_COMPENSATION,*/
                     SUM(NSS_X_RATINGS) / SUM(NO_OF_RATINGS)                      AS NSS,
                     SUM(CL_REACTIVES_ROI_CHURN_V2.REOPEN)                           AS REOPEN,
                     SUM(0)                                          AS CLAIMED_VALUE,
                     SUM(COMPENSATION_VALUE_USD)                                   as USD_COMPENSATIONS,
                     SUM(AUTOMATIC_COMPENSATION_USD)                                  AS AUTOMATIC_COMPENSATION,
                     SUM(ALLY_COMPENSATIONS)
                                            AS PAID_BY_ALLY,
                     SUM(AGENT_COMPENSATION_USD)
    AS MANUAL_COMPENSATION,
                     SUM(ORDERS)                                                  AS ORDERS,
                     SUM(CASE WHEN TICKET_TYPE <> 'Sin Ticket' THEN 1 ELSE 0 END) AS DEFECTS,
                     (SUM(GMV_AFTER_7_DAYS / TRM.TRM)) / SUM(ORDERS)             AS GMV_AFTER_7_DAYS,
                     (SUM(GMV_AFTER_7_DAYS_ORGANIC / TRM.TRM)) / SUM(ORDERS)     AS GMV_AFTER_7_DAYS_ORGANIC,
                     (SUM(GMV_AFTER_28_DAYS / TRM.TRM)) / SUM(ORDERS)            AS GMV_AFTER_28_DAYS,
                     (SUM(GMV_AFTER_28_DAYS_ORGANIC / TRM.TRM)) / SUM(ORDERS)    AS GMV_AFTER_28_DAYS_ORGANIC,
                     SUM(ORDER_MODIFICATION_DISCOUNTS)
                                           AS FREE_DELIVERY,
                     SUM(GO.TOTAL_VALUE / TRM.TRM)                                      AS TOTAL_VALUE_USD
              from ops_global.CL_REACTIVES_ROI_CHURN_V2
                       left join COMPENSATIONS cs on cs.ORDER_ID = CL_REACTIVES_ROI_CHURN_V2.ORDER_ID and cs.COUNTRY = 'CL'
                       left join pilot on pilot.ORDER_ID = CL_REACTIVES_ROI_CHURN_V2.ORDER_ID and pilot.COUNTRY = 'CL'
                       left join ops_global.global_orders go
                                 on go.ORDER_ID = CL_REACTIVES_ROI_CHURN_V2.ORDER_ID and go.COUNTRY = 'CL' and go.COUNT_TO_GMV
                            left join pick_up on pick_up.ORDER_ID = CL_REACTIVES_ROI_CHURN_V2.ORDER_ID and pick_up.COUNTRY = 'CL'
                            left join global_finances.TRM_FIXED trm on trm.COUNTRY_CODE = 'CL'

              where date_trunc(day, CL_REACTIVES_ROI_CHURN_V2.CREATED_AT)::date between current_date - interval '5 months' and current_date - interval '1 day'
              group by 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23

              union all

              select 'MX'                                                            COUNTRY,
                     CITY_NAME,
                     MX_REACTIVES_ROI_CHURN_V2.CREATED_AT::date as created_at,
                     MX_REACTIVES_ROI_CHURN_V2.ORDER_ID,
                     CS.TICKET_URL,
                     TICKET_TYPE,
                     TIPOLOGIA,
                     MX_REACTIVES_ROI_CHURN_V2.IS_AUTOMATED,
                     MX_REACTIVES_ROI_CHURN_V2.VERTICAL_GROUP,
                     MX_REACTIVES_ROI_CHURN_V2.VERTICAL_SUB_GROUP,
                     MX_REACTIVES_ROI_CHURN_V2.RFM_SEGMENT
                            AS SEGMENT_RFM,
                     MX_REACTIVES_ROI_CHURN_V2.FINANCE_USER_TYPE,
                     MX_REACTIVES_ROI_CHURN_V2.ITERATION   as treatment,
                     MX_REACTIVES_ROI_CHURN_V2.LEVEL_1 AS CATEGORY_LEVEL_1,
                     MX_REACTIVES_ROI_CHURN_V2.LEVEL_2             as SUB_CATEGORY_LEVEL_1,
                     MX_REACTIVES_ROI_CHURN_V2.APPLICATION_USER_ID                   as APPLICATION_USER_ID,
                     cs.LAST_AGENT_EMAIL  as last_agent,

                     CS.SOLUTION_TYPE,
                     TICKETAUTOMATIONEXITRULE as exit_rule,
                     exit_tipification,
go.payment_method, go.delivery_type, pick_up.delivery_method,
                  /*CASE WHEN GIVEN_VALUE_AUTOMATIC_ > 0 AND MONTO_COMPENSADO_LIVE - GIVEN_VALUE_AUTOMATIC_ > 0 THEN 'AUTOMATIC-MANUAL_COMPENSATION'
                          WHEN GIVEN_VALUE_AUTOMATIC_ > 0 THEN 'AUTOMATIC_COMPENSATION'
                          WHEN MONTO_COMPENSADO_LIVE - GIVEN_VALUE_AUTOMATIC_ > 0 THEN 'MANUAL_COMPENSATION'
                          ELSE NULL END AS TYPE_COMPENSATION,*/
                     SUM(NSS_X_RATINGS) / SUM(NO_OF_RATINGS)                      AS NSS,
                     SUM(MX_REACTIVES_ROI_CHURN_V2.REOPEN)                           AS REOPEN,
                     SUM(0)                                          AS CLAIMED_VALUE,
                     SUM(COMPENSATION_VALUE_USD)                                   as USD_COMPENSATIONS,
                     SUM(AUTOMATIC_COMPENSATION_USD)                                  AS AUTOMATIC_COMPENSATION,
                     SUM(ALLY_COMPENSATIONS)
                                            AS PAID_BY_ALLY,
                     SUM(AGENT_COMPENSATION_USD)
    AS MANUAL_COMPENSATION,
                     SUM(ORDERS)                                                  AS ORDERS,
                     SUM(CASE WHEN TICKET_TYPE <> 'Sin Ticket' THEN 1 ELSE 0 END) AS DEFECTS,
                     (SUM(GMV_AFTER_7_DAYS / TRM.TRM)) / SUM(ORDERS)             AS GMV_AFTER_7_DAYS,
                     (SUM(GMV_AFTER_7_DAYS_ORGANIC / TRM.TRM)) / SUM(ORDERS)     AS GMV_AFTER_7_DAYS_ORGANIC,
                     (SUM(GMV_AFTER_28_DAYS / TRM.TRM)) / SUM(ORDERS)            AS GMV_AFTER_28_DAYS,
                     (SUM(GMV_AFTER_28_DAYS_ORGANIC / TRM.TRM)) / SUM(ORDERS)    AS GMV_AFTER_28_DAYS_ORGANIC,
                     SUM(ORDER_MODIFICATION_DISCOUNTS)
                                           AS FREE_DELIVERY,
                     SUM(GO.TOTAL_VALUE / TRM.TRM)                                      AS TOTAL_VALUE_USD
              from ops_global.MX_REACTIVES_ROI_CHURN_V2
                       left join COMPENSATIONS cs on cs.ORDER_ID = MX_REACTIVES_ROI_CHURN_V2.ORDER_ID and cs.COUNTRY = 'MX'
                       left join pilot on pilot.ORDER_ID = MX_REACTIVES_ROI_CHURN_V2.ORDER_ID and pilot.COUNTRY = 'MX'
                       left join ops_global.global_orders go
                                 on go.ORDER_ID = MX_REACTIVES_ROI_CHURN_V2.ORDER_ID and go.COUNTRY = 'MX' and go.COUNT_TO_GMV
                            left join pick_up on pick_up.ORDER_ID = MX_REACTIVES_ROI_CHURN_V2.ORDER_ID and pick_up.COUNTRY = 'MX'
                            left join global_finances.TRM_FIXED trm on trm.COUNTRY_CODE = 'MX'

              where date_trunc(day, MX_REACTIVES_ROI_CHURN_V2.CREATED_AT)::date between current_date - interval '5 months' and current_date - interval '1 day'
              group by 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23

              union all

              select 'PE'                                                            COUNTRY,
                     CITY_NAME,
                     PE_REACTIVES_ROI_CHURN_V2.CREATED_AT::date as created_at,
                     PE_REACTIVES_ROI_CHURN_V2.ORDER_ID,
                     CS.TICKET_URL,
                     TICKET_TYPE,
                     TIPOLOGIA,
                     PE_REACTIVES_ROI_CHURN_V2.IS_AUTOMATED,
                     PE_REACTIVES_ROI_CHURN_V2.VERTICAL_GROUP,
                     PE_REACTIVES_ROI_CHURN_V2.VERTICAL_SUB_GROUP,
                     PE_REACTIVES_ROI_CHURN_V2.RFM_SEGMENT
                            AS SEGMENT_RFM,
                     PE_REACTIVES_ROI_CHURN_V2.FINANCE_USER_TYPE,
                     PE_REACTIVES_ROI_CHURN_V2.ITERATION   as treatment,
                     PE_REACTIVES_ROI_CHURN_V2.LEVEL_1 AS CATEGORY_LEVEL_1,
                     PE_REACTIVES_ROI_CHURN_V2.LEVEL_2             as SUB_CATEGORY_LEVEL_1,
                     PE_REACTIVES_ROI_CHURN_V2.APPLICATION_USER_ID                   as APPLICATION_USER_ID,
                     cs.LAST_AGENT_EMAIL  as last_agent,

                     CS.SOLUTION_TYPE,
                     TICKETAUTOMATIONEXITRULE as exit_rule,
                     exit_tipification,
go.payment_method, go.delivery_type, pick_up.delivery_method,
                  /*CASE WHEN GIVEN_VALUE_AUTOMATIC_ > 0 AND MONTO_COMPENSADO_LIVE - GIVEN_VALUE_AUTOMATIC_ > 0 THEN 'AUTOMATIC-MANUAL_COMPENSATION'
                          WHEN GIVEN_VALUE_AUTOMATIC_ > 0 THEN 'AUTOMATIC_COMPENSATION'
                          WHEN MONTO_COMPENSADO_LIVE - GIVEN_VALUE_AUTOMATIC_ > 0 THEN 'MANUAL_COMPENSATION'
                          ELSE NULL END AS TYPE_COMPENSATION,*/
                     SUM(NSS_X_RATINGS) / SUM(NO_OF_RATINGS)                      AS NSS,
                     SUM(PE_REACTIVES_ROI_CHURN_V2.REOPEN)                           AS REOPEN,
                     SUM(0)                                          AS CLAIMED_VALUE,
                     SUM(COMPENSATION_VALUE_USD)                                   as USD_COMPENSATIONS,
                     SUM(AUTOMATIC_COMPENSATION_USD)                                  AS AUTOMATIC_COMPENSATION,
                     SUM(ALLY_COMPENSATIONS)
                                            AS PAID_BY_ALLY,
                     SUM(AGENT_COMPENSATION_USD)
    AS MANUAL_COMPENSATION,
                     SUM(ORDERS)                                                  AS ORDERS,
                     SUM(CASE WHEN TICKET_TYPE <> 'Sin Ticket' THEN 1 ELSE 0 END) AS DEFECTS,
                     (SUM(GMV_AFTER_7_DAYS / TRM.TRM)) / SUM(ORDERS)             AS GMV_AFTER_7_DAYS,
                     (SUM(GMV_AFTER_7_DAYS_ORGANIC / TRM.TRM)) / SUM(ORDERS)     AS GMV_AFTER_7_DAYS_ORGANIC,
                     (SUM(GMV_AFTER_28_DAYS / TRM.TRM)) / SUM(ORDERS)            AS GMV_AFTER_28_DAYS,
                     (SUM(GMV_AFTER_28_DAYS_ORGANIC / TRM.TRM)) / SUM(ORDERS)    AS GMV_AFTER_28_DAYS_ORGANIC,
                     SUM(ORDER_MODIFICATION_DISCOUNTS)
                                           AS FREE_DELIVERY,
                     SUM(GO.TOTAL_VALUE / TRM.TRM)                                      AS TOTAL_VALUE_USD
              from ops_global.PE_REACTIVES_ROI_CHURN_V2
                       left join COMPENSATIONS cs on cs.ORDER_ID = PE_REACTIVES_ROI_CHURN_V2.ORDER_ID and cs.COUNTRY = 'PE'
                       left join pilot on pilot.ORDER_ID = PE_REACTIVES_ROI_CHURN_V2.ORDER_ID and pilot.COUNTRY = 'PE'
                       left join ops_global.global_orders go
                                 on go.ORDER_ID = PE_REACTIVES_ROI_CHURN_V2.ORDER_ID and go.COUNTRY = 'PE' and go.COUNT_TO_GMV
                            left join pick_up on pick_up.ORDER_ID = PE_REACTIVES_ROI_CHURN_V2.ORDER_ID and pick_up.COUNTRY = 'PE'
                            left join global_finances.TRM_FIXED trm on trm.COUNTRY_CODE = 'PE'

              where date_trunc(day, PE_REACTIVES_ROI_CHURN_V2.CREATED_AT)::date between current_date - interval '5 months' and current_date - interval '1 day'
              group by 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23

              union all

              select 'EC'                                                            COUNTRY,
                     CITY_NAME,
                     EC_REACTIVES_ROI_CHURN_V2.CREATED_AT::date as created_at,
                     EC_REACTIVES_ROI_CHURN_V2.ORDER_ID,
                     CS.TICKET_URL,
                     TICKET_TYPE,
                     TIPOLOGIA,
                     EC_REACTIVES_ROI_CHURN_V2.IS_AUTOMATED,
                     EC_REACTIVES_ROI_CHURN_V2.VERTICAL_GROUP,
                     EC_REACTIVES_ROI_CHURN_V2.VERTICAL_SUB_GROUP,
                     EC_REACTIVES_ROI_CHURN_V2.RFM_SEGMENT
                            AS SEGMENT_RFM,
                     EC_REACTIVES_ROI_CHURN_V2.FINANCE_USER_TYPE,
                     EC_REACTIVES_ROI_CHURN_V2.ITERATION   as treatment,
                     EC_REACTIVES_ROI_CHURN_V2.LEVEL_1 AS CATEGORY_LEVEL_1,
                     EC_REACTIVES_ROI_CHURN_V2.LEVEL_2             as SUB_CATEGORY_LEVEL_1,
                     EC_REACTIVES_ROI_CHURN_V2.APPLICATION_USER_ID                   as APPLICATION_USER_ID,
                     cs.LAST_AGENT_EMAIL  as last_agent,

                     CS.SOLUTION_TYPE,
                     TICKETAUTOMATIONEXITRULE as exit_rule,
                     exit_tipification,
go.payment_method, go.delivery_type, pick_up.delivery_method,
                  /*CASE WHEN GIVEN_VALUE_AUTOMATIC_ > 0 AND MONTO_COMPENSADO_LIVE - GIVEN_VALUE_AUTOMATIC_ > 0 THEN 'AUTOMATIC-MANUAL_COMPENSATION'
                          WHEN GIVEN_VALUE_AUTOMATIC_ > 0 THEN 'AUTOMATIC_COMPENSATION'
                          WHEN MONTO_COMPENSADO_LIVE - GIVEN_VALUE_AUTOMATIC_ > 0 THEN 'MANUAL_COMPENSATION'
                          ELSE NULL END AS TYPE_COMPENSATION,*/
                     SUM(NSS_X_RATINGS) / SUM(NO_OF_RATINGS)                      AS NSS,
                     SUM(EC_REACTIVES_ROI_CHURN_V2.REOPEN)                           AS REOPEN,
                     SUM(0)                                          AS CLAIMED_VALUE,
                     SUM(COMPENSATION_VALUE_USD)                                   as USD_COMPENSATIONS,
                     SUM(AUTOMATIC_COMPENSATION_USD)                                  AS AUTOMATIC_COMPENSATION,
                     SUM(ALLY_COMPENSATIONS)
                                            AS PAID_BY_ALLY,
                     SUM(AGENT_COMPENSATION_USD)
    AS MANUAL_COMPENSATION,
                     SUM(ORDERS)                                                  AS ORDERS,
                     SUM(CASE WHEN TICKET_TYPE <> 'Sin Ticket' THEN 1 ELSE 0 END) AS DEFECTS,
                     SUM(GMV_AFTER_7_DAYS / TRM.TRM) / SUM(ORDERS)               AS GMV_AFTER_7_DAYS,
                     SUM(GMV_AFTER_7_DAYS_ORGANIC / TRM.TRM) / SUM(ORDERS)       AS GMV_AFTER_7_DAYS_ORGANIC,
                     SUM(GMV_AFTER_28_DAYS / TRM.TRM) / SUM(ORDERS)              AS GMV_AFTER_28_DAYS,
                     SUM(GMV_AFTER_28_DAYS_ORGANIC / TRM.TRM) / SUM(ORDERS)      AS GMV_AFTER_28_DAYS_ORGANIC,
                     SUM(ORDER_MODIFICATION_DISCOUNTS)
                                           AS FREE_DELIVERY,
                     SUM(GO.TOTAL_VALUE / TRM.TRM)                                      AS TOTAL_VALUE_USD
              from ops_global.EC_REACTIVES_ROI_CHURN_V2
                       left join COMPENSATIONS cs on cs.ORDER_ID = EC_REACTIVES_ROI_CHURN_V2.ORDER_ID and cs.COUNTRY = 'EC'
                       left join pilot on pilot.ORDER_ID = EC_REACTIVES_ROI_CHURN_V2.ORDER_ID and pilot.COUNTRY = 'EC'
                       left join ops_global.global_orders go
                                 on go.ORDER_ID = EC_REACTIVES_ROI_CHURN_V2.ORDER_ID and go.COUNTRY = 'EC' and go.COUNT_TO_GMV
                            left join pick_up on pick_up.ORDER_ID = EC_REACTIVES_ROI_CHURN_V2.ORDER_ID and pick_up.COUNTRY = 'EC'
                            left join global_finances.TRM_FIXED trm on trm.COUNTRY_CODE = 'EC'

              where date_trunc(day, EC_REACTIVES_ROI_CHURN_V2.CREATED_AT)::date between current_date - interval '5 months' and current_date - interval '1 day'
              group by 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23

              union all

              select 'UY'                                                            COUNTRY,
                     CITY_NAME,
                     UY_REACTIVES_ROI_CHURN_V2.CREATED_AT::date as created_at,
                     UY_REACTIVES_ROI_CHURN_V2.ORDER_ID,
                     CS.TICKET_URL,
                     TICKET_TYPE,
                     TIPOLOGIA,
                     UY_REACTIVES_ROI_CHURN_V2.IS_AUTOMATED,
                     UY_REACTIVES_ROI_CHURN_V2.VERTICAL_GROUP,
                     UY_REACTIVES_ROI_CHURN_V2.VERTICAL_SUB_GROUP,
                     UY_REACTIVES_ROI_CHURN_V2.RFM_SEGMENT
                            AS SEGMENT_RFM,
                     UY_REACTIVES_ROI_CHURN_V2.FINANCE_USER_TYPE,
                     UY_REACTIVES_ROI_CHURN_V2.ITERATION   as treatment,
                     UY_REACTIVES_ROI_CHURN_V2.LEVEL_1 AS CATEGORY_LEVEL_1,
                     UY_REACTIVES_ROI_CHURN_V2.LEVEL_2             as SUB_CATEGORY_LEVEL_1,
                     UY_REACTIVES_ROI_CHURN_V2.APPLICATION_USER_ID                   as APPLICATION_USER_ID,
                     cs.LAST_AGENT_EMAIL  as last_agent,

                     CS.SOLUTION_TYPE,
                     TICKETAUTOMATIONEXITRULE as exit_rule,
                     exit_tipification,
go.payment_method, go.delivery_type, pick_up.delivery_method,
                  /*CASE WHEN GIVEN_VALUE_AUTOMATIC_ > 0 AND MONTO_COMPENSADO_LIVE - GIVEN_VALUE_AUTOMATIC_ > 0 THEN 'AUTOMATIC-MANUAL_COMPENSATION'
                          WHEN GIVEN_VALUE_AUTOMATIC_ > 0 THEN 'AUTOMATIC_COMPENSATION'
                          WHEN MONTO_COMPENSADO_LIVE - GIVEN_VALUE_AUTOMATIC_ > 0 THEN 'MANUAL_COMPENSATION'
                          ELSE NULL END AS TYPE_COMPENSATION,*/
                     SUM(NSS_X_RATINGS) / SUM(NO_OF_RATINGS)                      AS NSS,
                     SUM(UY_REACTIVES_ROI_CHURN_V2.REOPEN)                           AS REOPEN,
                     SUM(0)                                          AS CLAIMED_VALUE,
                     SUM(COMPENSATION_VALUE_USD)                                   as USD_COMPENSATIONS,
                     SUM(AUTOMATIC_COMPENSATION_USD)                                  AS AUTOMATIC_COMPENSATION,
                     SUM(ALLY_COMPENSATIONS)
                                            AS PAID_BY_ALLY,
                     SUM(AGENT_COMPENSATION_USD)
    AS MANUAL_COMPENSATION,
                     SUM(ORDERS)                                                  AS ORDERS,
                     SUM(CASE WHEN TICKET_TYPE <> 'Sin Ticket' THEN 1 ELSE 0 END) AS DEFECTS,
                     SUM(GMV_AFTER_7_DAYS / TRM.TRM) / SUM(ORDERS)               AS GMV_AFTER_7_DAYS,
                     SUM(GMV_AFTER_7_DAYS_ORGANIC / TRM.TRM) / SUM(ORDERS)       AS GMV_AFTER_7_DAYS_ORGANIC,
                     SUM(GMV_AFTER_28_DAYS / TRM.TRM) / SUM(ORDERS)              AS GMV_AFTER_28_DAYS,
                     SUM(GMV_AFTER_28_DAYS_ORGANIC / TRM.TRM) / SUM(ORDERS)      AS GMV_AFTER_28_DAYS_ORGANIC,
                     SUM(ORDER_MODIFICATION_DISCOUNTS)
                                           AS FREE_DELIVERY,
                     SUM(GO.TOTAL_VALUE / TRM.TRM)                                      AS TOTAL_VALUE_USD
              from ops_global.UY_REACTIVES_ROI_CHURN_V2
                       left join COMPENSATIONS cs on cs.ORDER_ID = UY_REACTIVES_ROI_CHURN_V2.ORDER_ID and cs.COUNTRY = 'UY'
                       left join pilot on pilot.ORDER_ID = UY_REACTIVES_ROI_CHURN_V2.ORDER_ID and pilot.COUNTRY = 'UY'
                       left join ops_global.global_orders go
                                 on go.ORDER_ID = UY_REACTIVES_ROI_CHURN_V2.ORDER_ID and go.COUNTRY = 'UY' and go.COUNT_TO_GMV
              left join pick_up on pick_up.ORDER_ID = UY_REACTIVES_ROI_CHURN_V2.ORDER_ID and pick_up.COUNTRY = 'UY'
              left join global_finances.TRM_FIXED trm on trm.COUNTRY_CODE = 'UY'
              where date_trunc(day, UY_REACTIVES_ROI_CHURN_V2.CREATED_AT)::date between current_date - interval '5 months' and current_date - interval '1 day'
              group by 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23

              union all

              select 'CR'                                                            COUNTRY,
                     CITY_NAME,
                     CR_REACTIVES_ROI_CHURN_V2.CREATED_AT::date as created_at,
                     CR_REACTIVES_ROI_CHURN_V2.ORDER_ID,
                     CS.TICKET_URL,
                     TICKET_TYPE,
                     TIPOLOGIA,
                     CR_REACTIVES_ROI_CHURN_V2.IS_AUTOMATED,
                     CR_REACTIVES_ROI_CHURN_V2.VERTICAL_GROUP,
                     CR_REACTIVES_ROI_CHURN_V2.VERTICAL_SUB_GROUP,
                     CR_REACTIVES_ROI_CHURN_V2.RFM_SEGMENT
                            AS SEGMENT_RFM,
                     CR_REACTIVES_ROI_CHURN_V2.FINANCE_USER_TYPE,
                     CR_REACTIVES_ROI_CHURN_V2.ITERATION   as treatment,
                     CR_REACTIVES_ROI_CHURN_V2.LEVEL_1 AS CATEGORY_LEVEL_1,
                     CR_REACTIVES_ROI_CHURN_V2.LEVEL_2             as SUB_CATEGORY_LEVEL_1,
                     CR_REACTIVES_ROI_CHURN_V2.APPLICATION_USER_ID                   as APPLICATION_USER_ID,
                     cs.LAST_AGENT_EMAIL  as last_agent,

                     CS.SOLUTION_TYPE,
                     TICKETAUTOMATIONEXITRULE as exit_rule,
                     exit_tipification,
go.payment_method, go.delivery_type, pick_up.delivery_method,
                  /*CASE WHEN GIVEN_VALUE_AUTOMATIC_ > 0 AND MONTO_COMPENSADO_LIVE - GIVEN_VALUE_AUTOMATIC_ > 0 THEN 'AUTOMATIC-MANUAL_COMPENSATION'
                          WHEN GIVEN_VALUE_AUTOMATIC_ > 0 THEN 'AUTOMATIC_COMPENSATION'
                          WHEN MONTO_COMPENSADO_LIVE - GIVEN_VALUE_AUTOMATIC_ > 0 THEN 'MANUAL_COMPENSATION'
                          ELSE NULL END AS TYPE_COMPENSATION,*/
                     SUM(NSS_X_RATINGS) / SUM(NO_OF_RATINGS)                      AS NSS,
                     SUM(CR_REACTIVES_ROI_CHURN_V2.REOPEN)                           AS REOPEN,
                     SUM(0)                                          AS CLAIMED_VALUE,
                     SUM(COMPENSATION_VALUE_USD)                                   as USD_COMPENSATIONS,
                     SUM(AUTOMATIC_COMPENSATION_USD)                                  AS AUTOMATIC_COMPENSATION,
                     SUM(ALLY_COMPENSATIONS)
                                            AS PAID_BY_ALLY,
                     SUM(AGENT_COMPENSATION_USD)
    AS MANUAL_COMPENSATION,
                     SUM(ORDERS)                                                  AS ORDERS,
                     SUM(CASE WHEN TICKET_TYPE <> 'Sin Ticket' THEN 1 ELSE 0 END) AS DEFECTS,
                     SUM(GMV_AFTER_7_DAYS / TRM.TRM) / SUM(ORDERS)               AS GMV_AFTER_7_DAYS,
                     SUM(GMV_AFTER_7_DAYS_ORGANIC / TRM.TRM) / SUM(ORDERS)       AS GMV_AFTER_7_DAYS_ORGANIC,
                     SUM(GMV_AFTER_28_DAYS / TRM.TRM) / SUM(ORDERS)              AS GMV_AFTER_28_DAYS,
                     SUM(GMV_AFTER_28_DAYS_ORGANIC / TRM.TRM) / SUM(ORDERS)      AS GMV_AFTER_28_DAYS_ORGANIC,
                     SUM(ORDER_MODIFICATION_DISCOUNTS)
                                           AS FREE_DELIVERY,
                     SUM(GO.TOTAL_VALUE / TRM.TRM)                                      AS TOTAL_VALUE_USD

              from ops_global.CR_REACTIVES_ROI_CHURN_V2
                       left join COMPENSATIONS cs on cs.ORDER_ID = CR_REACTIVES_ROI_CHURN_V2.ORDER_ID and cs.COUNTRY = 'CR'
                       left join pilot on pilot.ORDER_ID = CR_REACTIVES_ROI_CHURN_V2.ORDER_ID and pilot.COUNTRY = 'CR'
                       left join ops_global.global_orders go
                                 on go.ORDER_ID = CR_REACTIVES_ROI_CHURN_V2.ORDER_ID and go.COUNTRY = 'CR' and go.COUNT_TO_GMV
                       left join pick_up on pick_up.ORDER_ID = CR_REACTIVES_ROI_CHURN_V2.ORDER_ID and pick_up.COUNTRY = 'CR'
                       left join global_finances.TRM_FIXED trm on trm.COUNTRY_CODE = 'CR'
              where date_trunc(day, CR_REACTIVES_ROI_CHURN_V2.CREATED_AT)::date between current_date - interval '5 months' and current_date - interval '1 day'
              group by 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23
          ),
          DUMP AS (
              SELECT COUNTRIES_UNION_ALL.*,
                     COALESCE(RD.DEBITED_VALUE_USD * (-1), 0)                                    AS RAPPI_CREDITS,
                     DATE_TRUNC(DAY, CREATED_AT)                                                 AS FECHA,
                     'DAILY'                                                                     AS CALENDAR_TYPE,
                     CASE
                         WHEN MANUAL_COMPENSATION > 0 AND AUTOMATIC_COMPENSATION > 0
                             THEN 'AUTOMATIC-MANUAL_COMPENSATION'
                         WHEN AUTOMATIC_COMPENSATION > 0 THEN 'AUTOMATIC_COMPENSATION'
                         WHEN MANUAL_COMPENSATION > 0 THEN 'MANUAL_COMPENSATION'
                         ELSE NULL END                                                            AS TYPE_COMPENSATION,
            
                                     case
                    when A.CATEGORY in ('DIAMANTE') then 'DIAMOND'
                    when A.CATEGORY in ('OURO', 'ORO') then 'GOLD'
                    when A.CATEGORY in ('PRATA', 'PLATA')  then 'SILVER'
                    when A.CATEGORY in ('BRONZE', 'BRONCE')  then 'BRONZE'
                end                                                      as loyalty,
                                                                                        
                     COALESCE((SUM(RAPPI_CREDITS) + SUM(TOTAL_VALUE_USD)), SUM(TOTAL_VALUE_USD)) AS TOTAL_VALUE_RC_USD


              FROM COUNTRIES_UNION_ALL
                       LEFT JOIN (select DEBITED_ORDER_ID, country, sum(DEBITED_VALUE_USD) as DEBITED_VALUE_USD from GLOBAL_FINANCES.GLOBAL_RAPPI_CREDIT_DEBITED
                           group by 1,2) RD
                                 on RD.DEBITED_ORDER_ID = COUNTRIES_UNION_ALL.ORDER_ID and
                                    RD.COUNTRY = COUNTRIES_UNION_ALL.COUNTRY
                       LEFT JOIN (SELECT *,
                                         TIMEADD(SECOND, -1,
                                                 CASE WHEN MC_ACTUAL = 1 THEN CURRENT_DATE + 1 ELSE END_DATE END) END_DATE_
                                  FROM GLOBAL_FINANCES.GLOBAL_APPLICATION_USERS_HIST) A
                                 ON A.COUNTRY = COUNTRIES_UNION_ALL.COUNTRY AND
                                    A.ID = COUNTRIES_UNION_ALL.APPLICATION_USER_ID AND
                                    COUNTRIES_UNION_ALL.CREATED_AT::TIMESTAMP BETWEEN A.START_DATE::TIMESTAMP AND A.END_DATE_::TIMESTAMP
              WHERE TIPOLOGIA <> 'Sin Ticket'
              GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26,
                       27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42 ,43
          ),
          DUMP_FINAL AS (
              SELECT *,
                     ROUND(SUM(MANUAL_COMPENSATION) / NULLIF(SUM(TOTAL_VALUE_RC_USD), 0), 2) AS COMPENSATION_PERC

              FROM DUMP
              GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26,
                       27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42 ,43 ,44
          )
     SELECT *,      CASE
                         WHEN VERTICAL_GROUP = 'RESTAURANTS' AND COMPENSATION_PERC > 1.1  
                         AND TICKET_TYPE IN ('1) Pedido no llegó','2) Falta un item','3) Producto en mal estado',
                                '4) Item equivocado','5) Other')                                                            THEN 'Sobre Compensado 110% Restaurantes'
                         
                         
                         WHEN (VERTICAL_GROUP = 'CPGS' AND dump_final.LOYALTY in ('SILVER','BRONZE') 
                               AND COMPENSATION_PERC > 0.5 
                               AND SUB_CATEGORY_LEVEL_1 = 'different product')                                              THEN 'Sobre compensado Diferente CPGs'
                               
                         WHEN (VERTICAL_GROUP = 'CPGS' AND dump_final.LOYALTY in ('SILVER','BRONZE') AND COMPENSATION_PERC > 0.5 
                               AND SUB_CATEGORY_LEVEL_1 = 'Product in poor condition')                                      THEN 'Sobre compensado Mal estado CPGs'
                               
                         WHEN (CATEGORY_LEVEL_1 = 'Order Never Arrived' 
                               AND PAYMENT_METHOD = 'cash'
                               AND TICKET_TYPE IN ('1) Pedido no llegó','5) Other') 
                               AND EXIT_TIPIFICATION = 'NOT_DELIVERED_ORDER')                                               THEN 'Sobre compensado No llego en efectivo'
                               
                         WHEN COMPENSATION_PERC > 1.1 AND TICKET_TYPE IN ('1) Pedido no llegó','2) Falta un item','3) Producto en mal estado',
                                '4) Item equivocado','5) Other')                                                            THEN 'Sobre compensado 110%'
                                
                         WHEN DELIVERY_METHOD = 'pickup' AND COMPENSATION_PERC > 0 
                         AND TICKET_TYPE IN ('1) Pedido no llegó','2) Falta un item','3) Producto en mal estado',
                                '4) Item equivocado','5) Other')                                                            THEN 'Compensación Pickup Error'
                         ELSE 'Compensación OK' END                                                                                                                                   AS COMPENSATION_CLASS,
                         
                     CASE
                         WHEN COMPENSATION_CLASS IN
                              ('Sobre Compensado 110% Restaurantes', 'Sobre compensado Diferente CPGs',
                               'Sobre compensado Mal estado CPGs', 'Sobre compensado No llego en efectivo',
                               'Sobre compensado 110%','Compensación Pickup Error')
                             THEN TRUE
                         ELSE FALSE END                                                                                                                                               AS SOBRE_COMPENSADOS,
                         

                    CASE WHEN DELIVERY_METHOD = 'pickup' AND COMPENSATION_PERC > 0 AND TICKET_TYPE IN ('1) Pedido no llegó','2) Falta un item','3) Producto en mal estado',
                                '4) Item equivocado','5) Other')                                                             THEN MANUAL_COMPENSATION
                         WHEN COMPENSATION_PERC > 1.1 AND TICKET_TYPE IN ('1) Pedido no llegó','2) Falta un item','3) Producto en mal estado',
                                '4) Item equivocado','5) Other')                                                             THEN MANUAL_COMPENSATION-TOTAL_VALUE_RC_USD
                         WHEN VERTICAL_GROUP = 'RESTAURANTS' AND COMPENSATION_PERC > 1.1  
                         AND TICKET_TYPE IN ('1) Pedido no llegó','2) Falta un item','3) Producto en mal estado',
                                '4) Item equivocado','5) Other')                                                             THEN MANUAL_COMPENSATION-TOTAL_VALUE_RC_USD

                         WHEN (CATEGORY_LEVEL_1 = 'Order Never Arrived' AND PAYMENT_METHOD = 'cash'
                         AND TICKET_TYPE IN ('1) Pedido no llegó','5) Other') AND EXIT_TIPIFICATION = 'NOT_DELIVERED_ORDER'
                         )                                                                                                   THEN MANUAL_COMPENSATION
                         WHEN (VERTICAL_GROUP = 'CPGS' AND dump_final.LOYALTY in ('SILVER','BRONZE') 
                               AND COMPENSATION_PERC > 0.5 
                               AND SUB_CATEGORY_LEVEL_1 = 'different product')                                              THEN MANUAL_COMPENSATION-round(TOTAL_VALUE_RC_USD/2,2)
                               
                         WHEN (VERTICAL_GROUP = 'CPGS' AND dump_final.LOYALTY in ('SILVER','BRONZE') AND COMPENSATION_PERC > 0.5 
                               AND SUB_CATEGORY_LEVEL_1 = 'Product in poor condition')                                      THEN MANUAL_COMPENSATION-round(TOTAL_VALUE_RC_USD/2,2)
                         

                                END                                                                                                                                                      AS MONTO_EXTRA_COMPENSADO
     FROM DUMP_FINAL  